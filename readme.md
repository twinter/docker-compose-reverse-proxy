# docker-compose reverse proxy

A reverse proxy based on nginx to allow hosting multiple services on a single IP but with multiple hostnames in a docker based environment.
The assignment of hostnames to containers happens in the docker-compose file of the target container. The routing to these, generation of a SSL certificate and encryption happen automatically. The isolation between other containers and the internet through dockers internal networks (mostly the default network) prevent  access to other containers like databases, caches, etc.

## Usage

### Do not define other port mappings!

Do not configure port mappings on other containers/docker-compose files as these will not go through the reverse proxy!

Be especially careful not to expose any otherwise unprotected services like databases to the internet by defining port mappings.

### Running the proxy

Do not have something else running on ports 80 and 443.

Clone the repo with `git clone git@gitlab.com:twinter/docker-compose-reverse-proxy.git ~/docker/nginx-proxy`. The project will be cloned into a folder named nginx-proxy.

#### With the [docker-compose systemd service](https://gitlab.com/twinter/docker-compose-systemd-service) (recommended for permanent use)

This assumes you already have the systemd service set up.

- create a symlink to the projects folder in /etc/docker/compose with `sudo ln -s /home/<user>/docker/nginx-proxy /etc/docker/compose/nginx-proxy`
- enable and start the project with `sudo systemctl enable --now docker-compose@nginx-proxy`

#### With only docker-compose (recommended for testing)

Run `docker-compose up` in the projects root folder.

### Configuration of other proxyed containers

As noted above: do not define port mappings for your containers if you don't want to expose them to the internet!

Add the following environment variables to containers that should be rechable on a certain hostname:
- `VIRTUAL_HOST=<hostname>`
- (optional) `VIRTUAL_PORT=<port>` if you want to expose a specific port
- `LETSENCRYPT_HOST=<hostname>`
- `LETSENCRYPT_EMAIL=<valid email address>`

Expose a port to the internal network with the `expose` directive either in the Dockerfile or in the docker-compose file.

Use the network created by this project in other containers with something like the following settings in the docker-compose file:
```
services:
  app:
    ...
    networks:
      - proxy-tier
      - default  # connect to implicit internal network with the other containers in this file

...

networks:
  proxy-tier:
    external:
      name: nginxproxy_proxy-tier
```
This assumes the reverse proxy sits in a folder named nginx-proxy. You may have to change the external.name settings to match your setup.

Have a look at the following projects for more details:
- https://github.com/nginx-proxy/nginx-proxy for the reverse proxy
- https://github.com/nginx-proxy/acme-companion for the letsencrypt companion container
